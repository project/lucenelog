<?php

/**
 * @file
 * Template file to theme search result.
 */

  $query_param = drupal_get_query_parameters();
  $page = (isset($query_param['page']) && is_numeric($query_param['page'])) ? $query_param['page'] : 0;
  $limit = variable_get('lucenelog_pager_limit', 10);
  $index = ($page * $limit) + 1;
?>
<div id="lucenelog">
<?php foreach ($logs as $log): ?>
  <div class="log clearfix <?php ($index & 1) ? print 'odd' : print 'even'; ?>">
    <div class="log-num"><?php print $index++; ?></div>
    <div class="log-body">
      <div class="log-message">
        <div class='short-log-message'>
          <?php print truncate_utf8($log['message'], 90, FALSE, TRUE); ?>
        </div>
        <div class='full-log-message'>
          <?php print $log['message']; ?>
        </div>
      </div>
      <div class="log-metadata">
        <div>
          <?php print date('M d Y h:i:s', $log['timestamp']); ?>
        </div>
        <div class="log-separator">|</div>
        <div>
          <?php print '<b>type:</b> ' . $log['type']; ?>
        </div>
        <div class="log-separator">|</div>
        <div>
          <?php print '<b>username:</b> ' . $log['username'] . '(' . $log['uid'] . ')'; ?>
        </div>
        <div class="log-separator">|</div>
        <div>
          <?php print '<b>severity:</b> ' . $log['severity']; ?>
        </div>
      </div>
      <div>
        <?php print '<b>request_uri:</b> ' . $log['request_uri']; ?>
      </div>
      <div class="log-hidden-data clearfix">
        <div>
          <?php print '<b>host:</b> ' . $log['host']; ?>
        </div>
        <div>
          <?php print '<b>referer:</b> ' . $log['referer']; ?>
        </div>
        <div>
          <?php print '<b>server_host:</b> ' . $log['server_host']; ?>
        </div>
        <div>
          <?php print '<b>client_host:</b> ' . $log['client_host']; ?>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
</div>
