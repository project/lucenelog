<?php

/**
 * @file
 * Settings file for lucenelog module.
 */

/**
 * Admin settings form.
 *
 * @param array $form
 *   form array.
 * @param array $form_state
 *   form_state array.
 *
 * @return array
 *   form array.
 */
function lucenelog_settings_form($form, &$form_state) {
  $form = array();
  $form['lucenelog_index_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Index path of lucene log'),
    '#required' => TRUE,
    '#default_value' => variable_get('lucenelog_index_path', 'sites/all/lucenelog_index'),
    '#description' => t('Type your lucenelog index path. For example sites/all/lucenelog_index.'),
  );
  $form['lucenelog_pager_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum results per page'),
    '#required' => TRUE,
    '#default_value' => variable_get('lucenelog_pager_limit', 10),
    '#description' => t('Type how many results should be displayed per page.'),
  );
  $form['lucenelog_performance'] = array(
    '#type' => 'fieldset',
    '#title' => t('Lucene log performance'),
  );
  $form['lucenelog_performance']['lucenelog_default_search_field'] = array(
    '#type' => 'select',
    '#title' => t('Default search field'),
    '#required' => TRUE,
    '#default_value' => variable_get('lucenelog_default_search_field', 'message'),
    '#options' => lucenelog_search_field(),
    '#description' => t('Select default search field.'),
  );
  $form['lucenelog_performance']['lucenelog_resultset_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Resultset limit'),
    '#required' => TRUE,
    '#default_value' => variable_get('lucenelog_resultset_limit', 100),
    '#description' => t("Type maximum resultset limit. Shouldn't be less than 100."),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validation handler of admin settings form.
 *
 * @param array $form
 *   form array.
 * @param array $form_state
 *   form_state array.
 */
function lucenelog_settings_form_validate($form, &$form_state) {
  $new_index_path = $form_state['values']['lucenelog_index_path'];
  $new_index_path = ($new_index_path[0] == '/') ? $new_index_path : DRUPAL_ROOT . '/' . $new_index_path;
  $old_index_path = variable_get('lucenelog_index_path', 'sites/all/lucenelog_index');
  $old_index_path = ($old_index_path[0] == '/') ? $old_index_path : DRUPAL_ROOT . '/' . $old_index_path;
  $resultset_limit = $form_state['values']['lucenelog_resultset_limit'];
  $result_per_page = $form_state['values']['lucenelog_pager_limit'];

  // Checking new index path folder exist and non empty.
  if ($new_index_path != $old_index_path && is_dir($new_index_path)) {
    if (count(glob("$new_index_path/*")) > 0) {
      form_set_error('lucenelog_index_path', t('The directory is not empty might be in use. To proceed please delete or empty the directory.'));
    }
    elseif (!is_writable($new_index_path)) {
      form_set_error('lucenelog_index_path', t('The !path is not writable.', array('!path' => $new_index_path)));
    }
  }

  // Validation for resultset limit.
  if (!is_numeric($resultset_limit)) {
    form_set_error('lucenelog_resultset_limit', t("Not a valid numeric value."));
  }
  elseif ($resultset_limit < 100) {
    form_set_error('lucenelog_resultset_limit', t("The resultset limit shouldn't be less than 100."));
  }

  // Validation for maximum results per page.
  if (!is_numeric($result_per_page)) {
    form_set_error('lucenelog_pager_limit', t("Not a valid numeric value."));
  }
  elseif ($result_per_page < 1) {
    form_set_error('lucenelog_pager_limit', t("Maximum results per page shouldn't be less than 1."));
  }
}

/**
 * Submit handler of admin settings form.
 *
 * @param array $form
 *   form array.
 * @param array $form_state
 *   form_state array.
 */
function lucenelog_settings_form_submit($form, &$form_state) {
  $new_raw_index_path = $form_state['values']['lucenelog_index_path'];
  $new_index_path = ($new_raw_index_path[0] == '/') ? $new_raw_index_path : DRUPAL_ROOT . '/' . $new_raw_index_path;
  $old_index_path = variable_get('lucenelog_index_path', 'sites/all/lucenelog_index');
  $old_index_path = ($old_index_path[0] == '/') ? $old_index_path : DRUPAL_ROOT . '/' . $old_index_path;
  $resultset_limit = $form_state['values']['lucenelog_resultset_limit'];
  $default_search_field = $form_state['values']['lucenelog_default_search_field'];
  $result_per_page = $form_state['values']['lucenelog_pager_limit'];

  // Saving new index path.
  if ($new_index_path != $old_index_path) {
    // Move files if old path folder exist and not empty.
    if (is_dir($old_index_path) && count(glob("$old_index_path/*")) > 0) {
      // Creating folder in new path.
      if (!is_dir($new_index_path)) {
        $status = lucenelog_createPath($new_index_path);
        if ($status['success'] === FALSE) {
          form_set_error('lucenelog_index_path', t('Not able to create folder. The path !path is not writable.', array('!path' => $status['path'])));
          return;
        }
      }
      // Moving folder from old_index_path to new_index_path.
      if (!rename($old_index_path, $new_index_path)) {
        form_set_error('lucenelog_index_path', t('There is some problem with the path.'));
      }
    }
    variable_set('lucenelog_index_path', $new_raw_index_path);
  }

  // Saving default search field.
  variable_set('lucenelog_default_search_field', $default_search_field);

  // Saving resultset limit.
  variable_set('lucenelog_resultset_limit', $resultset_limit);

  // Saving maximum results per page.
  variable_set('lucenelog_pager_limit', $result_per_page);

  drupal_set_message(t('The configurations has been saved successfully.'));
}

/**
 * Recursively create a long directory path.
 *
 * @param string $path
 *   Lucene index path.
 *
 * @return array
 *   Success and index path.
 */
function lucenelog_createPath($path) {
  if (is_dir($path)) {
    return TRUE;
  }
  $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1);
  $return = lucenelog_createPath($prev_path);
  return ($return && is_writable($prev_path)) ? array('success' => mkdir($path)) : array('success' => FALSE, 'path' => $prev_path);
}

/**
 * Returns all the available search fields.
 *
 * @return array
 *   Mapped search fields.
 */
function lucenelog_search_field() {
  $fields = array(
    'message' => t('message'),
    'type' => t('type'),
    'username' => t('username'),
    'uid' => t('uid'),
    'host' => t('host'),
    'severity' => t('severity'),
    'timestamp' => t('timestamp'),
    'referer' => t('referer'),
    'link' => t('link'),
    'request_uri' => t('request_uri'),
    'server_host' => t('server_host'),
    'client_host' => t('client_host'),
  );
  return $fields;
}
