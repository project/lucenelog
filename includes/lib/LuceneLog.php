<?php

/**
 * @file
 * This class defines the necessary functions to be used for adding and
 * searching lucene indexes.
 */

class LuceneLog {
  /**
   * Defines the constructor and adding composer manager auto loader.
   */
  public function __construct() {
    composer_manager_register_autoloader();
  }

  /**
   * Index the log entry and add it in lucene index.
   *
   * @param array $log_entry
   *   entry array supplied by watchdog hook.
   */
  public function addIndex(array $log_entry) {
    $index_path = variable_get('lucenelog_index_path', 'sites/all/lucenelog_index');
    $index_path = ($index_path[0] == '/') ? $index_path : DRUPAL_ROOT . '/' . $index_path;
    if (!class_exists('ZendSearch\Lucene\Lucene')) {
      return;
    }

    // Try to open index, on absent create it.
    try {
      $index = ZendSearch\Lucene\Lucene::open($index_path);
    } catch(ZendSearch\Lucene\Exception\RuntimeException $e) {
      $index = ZendSearch\Lucene\Lucene::create($index_path);
    }
    $message = strip_tags(is_array($log_entry['variables']) ? strtr($log_entry['message'], $log_entry['variables']) : $log_entry['message']);
    $username = isset($log_entry['user']->name) ? $log_entry['user']->name : variable_get('anonymous', t('Anonymous'));

    $document = new ZendSearch\Lucene\Document();
    $document->addField(ZendSearch\Lucene\Document\Field::Text('message', $message));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('type', $log_entry['type']));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('username', $username));
    $document->addField(ZendSearch\Lucene\Document\Field::Keyword('uid', $log_entry['user']->uid));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('host', php_uname('n')));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('severity', $this->decodeSeverity($log_entry['severity'])));
    $document->addField(ZendSearch\Lucene\Document\Field::Keyword('timestamp', $log_entry['timestamp']));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('referer', $log_entry['referer']));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('link', $log_entry['link']));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('request_uri', $log_entry['request_uri']));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('server_host', $_SERVER['HTTP_HOST']));
    $document->addField(ZendSearch\Lucene\Document\Field::Text('client_host', $log_entry['ip']));

    $index->addDocument($document);
    $index->optimize();
    $index->commit();
  }

  /**
   * Search key in the lucenelog index.
   *
   * @param string $key
   *   Query string.
   *
   * @return null|string
   *   Matched result.
   */
  public function searchIndex($key) {
    $index_path_var = variable_get('lucenelog_index_path', 'sites/all/lucenelog_index');
    $index_path = ($index_path_var[0] == '/') ? $index_path_var : DRUPAL_ROOT . '/' . $index_path_var;
    if (!class_exists('ZendSearch\Lucene\Lucene')) {
      return t('Either the index path !path not exist or lucene class not found.', array('!path' => $index_path_var));
    }
    elseif (!is_dir($index_path)) {
      return t('No watchdog messages indexed yet.');
    }

    try {
      $index = ZendSearch\Lucene\Lucene::open($index_path);
    } catch(ZendSearch\Lucene\Exception\RuntimeException $e) {
      return t("The index path !path doesn't contains any valid lucene index.", array('!path' => $index_path_var));
    }
    // Allow numbers in the range query.
    ZendSearch\Lucene\Analysis\Analyzer\Analyzer::setDefault(new ZendSearch\Lucene\Analysis\Analyzer\Common\TextNum\CaseInsensitive());
    // Setting default search field.
    ZendSearch\Lucene\Lucene::setDefaultSearchField(variable_get('lucenelog_default_search_field', 'message'));
    // Setting resultset limit.
    ZendSearch\Lucene\Lucene::setResultSetLimit(variable_get('lucenelog_resultset_limit', 100));
    $hits = $index->find($key);
    $items = array();
    $hits = $this->pagerArraySplice($hits);
    foreach ($hits as $hit) {
      $items[] = array(
        'message' => $hit->message,
        'type' => $hit->type,
        'username' => $hit->username,
        'uid' => $hit->uid,
        'host' => $hit->host,
        'severity' => $hit->severity,
        'timestamp' => $hit->timestamp,
        'referer' => $hit->referer,
        'link' => $hit->link,
        'request_uri' => $hit->request_uri,
        'server_host' => $hit->server_host,
        'client_host' => $hit->client_host,
      );
    }
    return $items;
  }

  /**
   * Provide severity string.
   *
   * @param integer $severity
   *   Severity integer.
   *
   * @return string
   *   Decoded severity.
   */
  protected function decodeSeverity($severity) {
    $severity_list = array(
      WATCHDOG_EMERGENCY => t('Emergency'),
      WATCHDOG_ALERT => t('Alert'),
      WATCHDOG_CRITICAL => t('Critical'),
      WATCHDOG_ERROR => t('Error'),
      WATCHDOG_WARNING => t('Warning'),
      WATCHDOG_NOTICE => t('Notice'),
      WATCHDOG_INFO => t('Info'),
      WATCHDOG_DEBUG => t('Debug'),
    );

    return isset($severity_list[$severity]) ? $severity_list[$severity] : 'NA';
  }
  /**
   * Function to extract a portion of array for pagination.
   *
   * @param array $data
   *   Apache full log data.
   *
   * @return array
   *   Apache log data sliced.
   */
  protected function pagerArraySplice($data) {
    global $pager_page_array, $pager_total, $pager_total_items;
    $limit = variable_get('lucenelog_pager_limit', 10);
    $page = isset($_GET['page']) ? $_GET['page'] : '';

    // Convert comma-separated $page to an array, used by other functions.
    $pager_page_array = explode(',', $page);

    // We calculate the total of pages as ceil(items / limit).
    $pager_total_items[0] = count($data);
    $pager_total[0] = ceil($pager_total_items[0] / $limit);
    $pager_page_array[0] = max(0, min((int) $pager_page_array[0], ((int) $pager_total[0]) - 1));
    return array_slice($data, $pager_page_array[0] * $limit, $limit, TRUE);
  }
}
