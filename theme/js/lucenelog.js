(function ($) {
  Drupal.behaviors.testPhpConfigBehavior = {
    attach:function (context) {
      $('#lucenelog .log').click(function() {
        $(this).find('.log-body .log-message .short-log-message').toggle();
        $(this).find('.log-body .log-message .full-log-message').toggle();
        $(this).find('.log-body .log-hidden-data').slideToggle();
      });
    }
  };
})(jQuery);