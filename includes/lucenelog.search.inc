<?php

/**
 * @file
 * Admin settings form.
 */

/**
 * Implements search form.
 *
 * @param array $form
 *   form array.
 * @param array $form_state
 *   form_state array.
 *
 * @return array
 *   form array.
 */
function lucenelog_search_form($form, $form_state) {
  $key = (isset($_SESSION['lucenelog_key'])) ? trim($_SESSION['lucenelog_key']) : NULL;
  $from = (isset($_SESSION['lucenelog_from'])) ? trim($_SESSION['lucenelog_from']) : NULL;
  $till = (isset($_SESSION['lucenelog_till'])) ? trim($_SESSION['lucenelog_till']) : NULL;

  $prepared_key = lucenelog_prepare_key($key, $from, $till);
  $form['key'] = array(
    '#type' => 'textfield',
    '#default_value' => $key,
    '#prefix' => '<div class="container-inline">',
    '#size' => 90,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#suffix' => '</div>',
  );

  $format = 'M d Y H:i';
  $form['from'] = array(
    '#type' => 'date_popup',
    '#title' => t('From'),
    '#default_value' => $from,
    '#date_format' => $format,
    '#date_label_position' => 'within',
    '#date_increment' => 15,
    '#date_year_range' => '-3:+3',
    '#prefix' => '<div id="lucenelog-timestamp">',
    '#size' => 13,
  );
  $form['till'] = array(
    '#type' => 'date_popup',
    '#title' => t('Till'),
    '#default_value' => $till,
    '#date_format' => $format,
    '#date_label_position' => 'within',
    '#date_increment' => 15,
    '#date_year_range' => '-3:+3',
    '#suffix' => '</div>',
    '#size' => 13,
  );

  if (isset($prepared_key) && $prepared_key != '') {
    $lucenelog = lucenelog_get_class();
    $result = array();
    try {
      $result = $lucenelog->searchIndex($prepared_key);
    }
    catch (Exception $e) {
      $error_message = $e->getMessage();
      $error_message = (empty($error_message)) ? t('Some unknown error occurs.') : $error_message;
      drupal_set_message($error_message, 'error');
    }
    $result = (!empty($result)) ? theme('lucenelog', array('logs' => $result)) : t('No result found.');
    $form['result'] = array(
      '#type' => 'item',
      '#markup' => $result,
    );
    $form['pager'] = array('#markup' => theme('pager'));
  }
  // Adding css and js files.
  drupal_add_css(drupal_get_path('module', 'lucenelog') . '/theme/css/lucenelog.css');
  drupal_add_js(drupal_get_path('module', 'lucenelog') . '/theme/js/lucenelog.js');
  return $form;
}

/**
 * Submit handler of the search form.
 *
 * @param array $form
 *   form array.
 * @param array $form_state
 *   form_state array.
 */
function lucenelog_search_form_submit($form, &$form_state) {
  $_SESSION['lucenelog_key'] = $form_state['values']['key'];
  $_SESSION['lucenelog_from'] = $form_state['values']['from'];
  $_SESSION['lucenelog_till'] = $form_state['values']['till'];
}

/**
 * Preparing key to send to lucene.
 *
 * @param string $key
 *   key string
 * @param string $from
 *   from date
 * @param string $till
 *   till date
 *
 * @return string
 *   prepared key ready to send to lucene.
 */
function lucenelog_prepare_key($key, $from, $till) {
  $prepared_key = (isset($key) && $key !='') ? $key : NULL;
  if (isset($from) || isset($till)) {
    $prepared_key = isset($prepared_key) ? "($key)" : NULL;
    $from = isset($from) ? strtotime($from) : 0;
    $till = isset($till) ? strtotime($till) : time();
    $timestamp = "timestamp:[$from TO $till]";
    $prepared_key .= isset($prepared_key) ? " AND $timestamp" : $timestamp;
  }
  return $prepared_key;
}
